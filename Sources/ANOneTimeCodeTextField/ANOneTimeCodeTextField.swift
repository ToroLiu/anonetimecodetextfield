//
//  ANOneTimeCodeTextField.swift
//  
//
//  Created by aecho on 2021/6/15.
//

import Foundation
import UIKit

import ANExtensions

public class ANOneTimeCodeTextField : UITextField, UITextFieldDelegate {
    
    var digitsLabelList: [UILabel] = []
    
    public override var text: String? {
        didSet {
            self.sendActions(for: .editingChanged)
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private var isConfigured: Bool = false
    private var actionLabelConfigure: ((UILabel) -> Void)?
    private var digitsFilledAction: ((Bool) -> Void)?
    
    public func configureLabelCustomizeAction(_ action: @escaping ((UILabel) -> Void)) {
        self.actionLabelConfigure = action
    }
    
    public func configureDigitsFilledAction(_ action: @escaping (Bool) -> Void) {
        self.digitsFilledAction = action
    }
    
    public func configureWith(count: Int) {
        
        guard self.isConfigured == false else {
            return
        }
        
        self.isConfigured = true
        
        self.configureTextField()
        
        let stackView = self.createStackViewWith(count)
        
        self.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.leftAnchor.constraint(equalTo: leftAnchor),
            stackView.rightAnchor.constraint(equalTo: rightAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
        stackView.invalidateIntrinsicContentSize()
        
        self.setNeedsUpdateConstraints()
        self.setNeedsLayout()
    }
    
    fileprivate var tapGesture: UITapGestureRecognizer {
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(becomeFirstResponder))
        return tap
    }
    
    fileprivate func configureTextField() {
        backgroundColor = .clear
        
        textColor = .clear
        tintColor = .clear
        keyboardType = .numberPad
        
        if #available(iOS 12.0, *) {
            textContentType = .oneTimeCode
        }
        
        self.addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
        
        self.addGestureRecognizer(self.tapGesture)
        
        self.delegate = self
    }
    
    fileprivate func createStackViewWith(_ count: Int) -> UIStackView {
        
        let stackView = UIStackView()
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .center
        stackView.distribution = .fillEqually
        stackView.axis = .horizontal
        stackView.spacing = 8
        
        for _ in 1 ... count {
            let label = UILabel()
            
            label.translatesAutoresizingMaskIntoConstraints = false
            
            label.textAlignment = .center
            
            // For cornerRadius
            label.clipsToBounds = true
            label.layer.cornerRadius = 10

            label.backgroundColor = UIColor(white: 231.0 / 255.0, alpha: 1.0)
            label.font = .systemFont(ofSize: 33, weight: .bold)
            label.isUserInteractionEnabled = true
            
            if let customize = self.actionLabelConfigure {
                customize(label)
            }
            self.digitsLabelList.append(label)
            
            stackView.addArrangedSubview(label)
            
            NSLayoutConstraint.activate([
                label.heightAnchor.constraint(equalTo: stackView.heightAnchor)
            ])
        }
        
        stackView.setNeedsUpdateConstraints()
        stackView.setNeedsLayout()
        
        return stackView
    }
    
    @objc fileprivate func textDidChanged() {
        
        let digitsCount = self.digitsLabelList.count
        
        let text = self.text ?? ""
        guard text.count <= digitsCount
        else { return }
        
        let textCount = text.count
        
        //
        for i in 0 ..< digitsCount {
            //
            let label = self.digitsLabelList[i]
            
            if (i < textCount) {
                let ch = text[i]
                label.text = ch
            } else {
                label.text = " "
            }
        }
        
        let isFilled = (digitsCount == textCount)
        if let action = self.digitsFilledAction {
            action(isFilled)
        }
    }
    
    // MARK: - UITextField Delegate
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text = (textField.text ?? "") as NSString
        
        let result = text.replacingCharacters(in: range, with: string)
        if (result.count <= digitsLabelList.count) {
            return true
        } else {
            return false
        }
    }
    
}
