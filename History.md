## v1.0.7
## Update: 2021/06/22

- UPDATE: 處理IOS 10的相容性問題。

--------

## v1.0.6
## Update: 2021/06/22

- UPDATE: 為了相容iOS 10，調整Package.swift的設定值。

--------

## v1.0.5
## Update: 2021/06/17

- UPDATE: 修正digitsFilledAction回傳值。帶回是否被filled的布林值。

--------

## v1.0.4
## Update: 2021/06/16

- UPDATE: 修正Label的底色。還是給個預設值。避免使用這元件，忘了設定底色。

--------

## v1.0.3
## Update: 2021/06/16

- UPDATE: 修正一開始，UILabel沒有顯示的問題。
- UPDATE: 修正Label的高度，應該要等同該TextField元件的高度。

--------

## v1.0.2
## Update: 2021/06/16

- UPDATE: 修正digitsFilledAction和labelCustomize的configure function。避免swift的語法衝突。

--------

## v1.0.1
## Update: 2021/06/16

- UPDATE: 將callback function的設定，各自獨立成函式。就不放在一起了。
- UPDATE: 處理auto layout exception的問題。
